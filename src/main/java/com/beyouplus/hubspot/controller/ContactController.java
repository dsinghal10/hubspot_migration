package com.beyouplus.hubspot.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beyouplus.hubspot.Client.ThirdPartyClient;
import com.beyouplus.hubspot.Repository.ContactDetailsRepository;
import com.beyouplus.hubspot.Service.ContactService;
import com.beyouplus.hubspot.ServiceImp.AgentsServiceImpl;
import com.beyouplus.hubspot.ServiceImp.PushNotesLS;
import com.beyouplus.hubspot.json.Request;
import com.beyouplus.hubspot.json.Response;
import com.beyouplus.hubspot.json.TaskPushRequest;
import com.beyouplus.hubspot.utility.Decoder;
import com.beyouplus.hubspot.utility.LogType;
import com.beyouplus.hubspot.utility.LogUtility;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("api")
public class ContactController {

	@Autowired
	ContactService contactService;
	@Autowired
	ContactDetailsRepository contactDetailsRepository;
	@Autowired
	ThirdPartyClient thirdPartyClient;
	@Autowired
	Decoder auth;
	@Value("${api.auth.key}")
	private String authKey;
	@Autowired
	AgentsServiceImpl agentsServiceImpl;
	@Autowired
	PushNotesLS pushNotesLS;

	@RequestMapping(value = "/test", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> test() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		long requestTimeMillis = System.currentTimeMillis();
		LogUtility.info("", "/test", LogType.REQUEST, "", "0", "REQUEST for test");
		Response response = new Response();

		try {
			LogUtility.debug("In service class");
			response.setResponseCode("00");
			response.setResponseMessage("Success");
			response.setResponseDescription("Welcome to BeYouPlus");

		} catch (Exception ex) {
			response.setResponseDescription("Something went wrong!");
			ex.printStackTrace();
		}
		LogUtility.info("", "/bodyparts", LogType.RESPONSE, mapper.writeValueAsString(response),
				String.valueOf(System.currentTimeMillis() - requestTimeMillis), "RESPONSE for tests.");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/contact", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> contactdetails(@RequestBody Request jsonrequest,
			@RequestHeader(name = "Authorization", required = true) String codeHeader) throws Exception {
		if (!(auth.decode(codeHeader).contentEquals(authKey))) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Response response = new Response();

		try {
			int noOfSavedContacts = contactService.saveContactDetails(jsonrequest);
			System.out.println("\n" + noOfSavedContacts + " new contacts are added successfully");
			response.setResponseCode("00");
			response.setResponseMessage("Success");
			response.setResponseDescription(noOfSavedContacts + " no of new contacts are added.");

		} catch (Exception ex) {
			response.setResponseDescription("Something went wrong!");
			ex.printStackTrace();
		}

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/agent", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> agentsdetails(@RequestBody Request jsonrequest,
			@RequestHeader(name = "Authorization", required = true) String codeHeader) throws Exception {
		if (!(auth.decode(codeHeader).contentEquals(authKey))) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Response response = new Response();

		try {
			int noOfSavedCouncellor = agentsServiceImpl.saveAgentsDetails();
			System.out.println("\n" + noOfSavedCouncellor + " new counsellor are added successfully");
			response.setResponseCode("00");
			response.setResponseMessage("Success");
			response.setResponseDescription(noOfSavedCouncellor + " no of new counsellor are added.");

		} catch (Exception ex) {
			response.setResponseMessage(ex.getMessage());
			response.setResponseDescription("Something went wrong!");
			ex.printStackTrace();
		}

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/task", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> pushTask(@Valid @RequestBody TaskPushRequest jsonrequest) throws Exception {
		Response response = new Response();
		try {
			String eresponse = pushNotesLS.pushTask(jsonrequest);
			response.setResponseCode("00");
			response.setResponseMessage("Success");
			response.setResponseDescription(eresponse);

		} catch (Exception ex) {
			response.setResponseMessage(ex.getMessage());
			response.setResponseDescription("Something went wrong!");
			ex.printStackTrace();
		}

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

}
