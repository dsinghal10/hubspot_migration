package com.beyouplus.hubspot.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.beyouplus.hubspot.model.Uploaddata;

@Repository
public interface UploaddataRepository extends JpaRepository<Uploaddata, Long> {

	@Query(value = "SELECT * FROM sampletable u WHERE u.id>:id  LIMIT 15", nativeQuery = true)
	public List<Uploaddata> getIDByLimit(Long id);
}
