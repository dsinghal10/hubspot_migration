package com.beyouplus.hubspot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beyouplus.hubspot.model.CounsellorMaster;

@Repository
public interface CounsellorMasterRepository extends JpaRepository<CounsellorMaster, Long> {
	CounsellorMaster findByAgentId(Long agentId);

}
