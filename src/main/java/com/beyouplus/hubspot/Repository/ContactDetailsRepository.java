package com.beyouplus.hubspot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beyouplus.hubspot.model.Contacts;

@Repository
public interface ContactDetailsRepository extends JpaRepository<Contacts, Long> {
	Contacts findBycustomerId(Long customerId);

}
