package com.beyouplus.hubspot.Client;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.beyouplus.hubspot.json.Request;

@Service
public class ThirdPartyClient {
	@Autowired
	RestTemplate restTemplate;

	public String restClientForGet(Request request, String data, String url) {
		System.out.println("REQUEST FROM GET API:" + url + data);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<Request> entity = new HttpEntity<Request>(request, headers);
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		ResponseEntity<String> response = restTemplate.exchange(url + data, HttpMethod.GET, entity, String.class);
		System.out.println("RESPONSE FROM GET API:" + response);
		return response.getBody();

	}

	public String restClientForPost(String request, String data, String url, String key) {
		System.out.println("REQUEST FROM POST API:" + request);

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		header.set(HttpHeaders.AUTHORIZATION, key);

		HttpEntity<String> httpEntity = new HttpEntity<String>(request, header);
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		System.out.println("RESPONSE FROM POST API:" + response);
		return response.getBody();
	}
}
