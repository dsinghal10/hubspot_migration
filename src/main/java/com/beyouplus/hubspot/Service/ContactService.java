package com.beyouplus.hubspot.Service;

import com.beyouplus.hubspot.json.Request;

public interface ContactService {
	int saveContactDetails(Request request) throws Exception;

	// boolean saveContactDetailsByVidOffset(Request request, Long vidOffset) throws
	// Exception;
}
