package com.beyouplus.hubspot.utility;

public enum LogType {
	REQUEST, RESPONSE, PUBLISH, CONSUME, EXCEPTION;

	private LogType() {
	}
}