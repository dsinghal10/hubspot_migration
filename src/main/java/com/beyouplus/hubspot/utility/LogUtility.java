package com.beyouplus.hubspot.utility;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class LogUtility {
	private static final Logger infoLog = LogManager.getLogger("infoLog");

	public LogUtility() {
	}

	public static void info(String httpStatus, String uri_topic, LogType logType, String data, String processingTime,
			String message) {
		String methodeName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String className = Thread.currentThread().getStackTrace()[2].getClassName();
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		String date = simpleDateFormat.format(timestamp);
		MDC.put("httpStatus", httpStatus);
		MDC.put("uri-topic", uri_topic);
		if (logType != null) {
			MDC.put("type", logType.name().toString());
		} else {
			MDC.put("type", (String) null);
		}

		MDC.put("timestamp", date);
		MDC.put("data", data);
		MDC.put("processingTime", processingTime);
		MDC.put("methodName", methodeName);
		MDC.put("className", className);
		MDC.put("exceptionMessage", "");
		MDC.put("exceptionStackTrace", "");
		infoLog.info(message);
	}

	public static void error(String httpStatus, String uri_topic, LogType logType, String data, String processingTime,
			String message, Exception e) {
		String methodeName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String className = Thread.currentThread().getStackTrace()[2].getClassName();
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		String date = simpleDateFormat.format(timestamp);
		MDC.put("httpStatus", httpStatus);
		MDC.put("uri-topic", uri_topic);
		if (logType != null) {
			MDC.put("type", logType.name().toString());
		} else {
			MDC.put("type", "");
		}

		MDC.put("timestamp", date);
		MDC.put("data", data);
		MDC.put("processingTime", processingTime);
		if (e != null) {
			MDC.put("exceptionMessage", e.getMessage());
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			MDC.put("exceptionStackTrace", sw.toString());
		} else {
			MDC.put("exceptionMessage", "");
			MDC.put("exceptionStackTrace", "");
		}

		MDC.put("methodName", methodeName);
		MDC.put("className", className);
		infoLog.error(message);
	}

	public static void debug(String message) {
		String methodeName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String className = Thread.currentThread().getStackTrace()[2].getClassName();
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		simpleDateFormat.format(timestamp);
		MDC.put("methodName", methodeName);
		MDC.put("className", className);
		MDC.put("httpStatus", "");
		MDC.put("uri-topic", "");
		MDC.put("type", "");
		MDC.put("timestamp", "");
		MDC.put("data", "");
		MDC.put("processingTime", "");
		MDC.put("exceptionMessage", "");
		MDC.put("exceptionStackTrace", "");
		if (infoLog.isDebugEnabled()) {
			infoLog.debug(message);
		}

	}
}