package com.beyouplus.hubspot.utility;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class MiliToTimestamp {
	public Timestamp parseMilitoTimestamp(Long miliseconds) {
		Date currentDate = new Date(miliseconds);
		// DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// System.out.println("Milliseconds to Date: " + df.format(currentDate));
		Timestamp timestamp = new Timestamp(currentDate.getTime());
		return timestamp;

	}

}
