package com.beyouplus.hubspot.utility;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component
public class Decoder {
	public String decode(String encodeCode) {
		String stringArray[] = encodeCode.split("\\s");
		String code = stringArray[1];
		byte[] byteArray = Base64.decodeBase64(code.getBytes());
		String decodedString = new String(byteArray);

		return decodedString;
	}
}
