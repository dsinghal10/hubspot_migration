package com.beyouplus.hubspot.ServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.beyouplus.hubspot.Client.ThirdPartyClient;
import com.beyouplus.hubspot.Repository.CounsellorMasterRepository;
import com.beyouplus.hubspot.json.CounsellorRequest;
import com.beyouplus.hubspot.model.CounsellorMaster;
import com.beyouplus.hubspot.utility.MiliToTimestamp;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AgentsServiceImpl {
	@Autowired
	ThirdPartyClient thirdPartyClient;
	@Autowired
	CounsellorMasterRepository counsellorMasterRepository;
	@Autowired
	MiliToTimestamp miliToTimestamp;
	@Value("${fetch.agents.details}")
	private String agentsDetailsURL;

	public int saveAgentsDetails() throws Exception {
		String agentsClientResponse = thirdPartyClient.restClientForGet(null, "", agentsDetailsURL);
		ObjectMapper mapper = new ObjectMapper();
		CounsellorRequest[] counsellorRequestList = mapper.readValue(agentsClientResponse, CounsellorRequest[].class);
		int noOfSavedCouncellor = modelBuilder(counsellorRequestList);
		return noOfSavedCouncellor;

	}

	int modelBuilder(CounsellorRequest[] counsellorRequestList) throws Exception {
		int agentSkipCounter = 0;
		int fetchAllAgentCount = counsellorRequestList.length;

		for (int i = 0; i < counsellorRequestList.length; i++) {
			if (counsellorRequestList[i].getOwnerId() != null) {
				CounsellorMaster counsellorMasterDb = counsellorMasterRepository
						.findByAgentId(counsellorRequestList[i].getOwnerId());
				if (counsellorMasterDb != null) {
					agentSkipCounter++;
					continue;
				}
			}
			CounsellorMaster counsellorMaster = new CounsellorMaster();
			counsellorMaster.setAgentId(counsellorRequestList[i].getOwnerId());
			counsellorMaster.setFirstName(counsellorRequestList[i].getFirstName());
			counsellorMaster.setAgentEmail(counsellorRequestList[i].getEmail());
			counsellorMaster.setLastName(counsellorRequestList[i].getLastName());
			counsellorMaster.setContactMobile("");
			counsellorMaster.setCreatedBy("Admin");

			counsellorMaster
					.setCreatedDate(miliToTimestamp.parseMilitoTimestamp(counsellorRequestList[i].getCreatedAt()));

			counsellorMaster
					.setLastUpdatedDate(miliToTimestamp.parseMilitoTimestamp(counsellorRequestList[i].getUpdatedAt()));
			counsellorMaster.setIsActive(counsellorRequestList[i].getIsActive());
			counsellorMasterRepository.saveAndFlush(counsellorMaster);

		}
		return (fetchAllAgentCount - agentSkipCounter);
	}
}
