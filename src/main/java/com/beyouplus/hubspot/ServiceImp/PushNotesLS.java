package com.beyouplus.hubspot.ServiceImp;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.beyouplus.hubspot.Client.ThirdPartyClient;
import com.beyouplus.hubspot.Repository.UploaddataRepository;
import com.beyouplus.hubspot.json.AssociationsResponse;
import com.beyouplus.hubspot.json.Contact;
import com.beyouplus.hubspot.json.EngagementResponse;
import com.beyouplus.hubspot.json.HubspotResponse;
import com.beyouplus.hubspot.json.Request;
import com.beyouplus.hubspot.json.TaskPushRequest;
import com.beyouplus.hubspot.model.Uploaddata;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;

@Service
public class PushNotesLS {
	@Autowired
	ThirdPartyClient thirdPartyClient;

	@Value("${fetch.details.byEnganementsId}")
	private String detailsByEnganementsIdURL;

	@Value("${hubspot.key}")
	private String hubspotKey;

	@Value("${get.details.byId}")
	private String detailsByContactIdURL;

	@Value("${search.details.byEmailId}")
	private String detailsByEmailIdURL;
	@Value("${task.engagement}")
	private String taskEngagementurl;
	ObjectMapper mapper = new ObjectMapper();
	@Value("${lead.push.activity}")
	private String lsPushActivityURL;
	Long count = 129304l;

	@Autowired
	UploaddataRepository uploaddataRepository;

	// @Scheduled(cron = "${scheduler.metrics}")
	public void getNotesAndContactIds() throws JsonMappingException, JsonProcessingException {
		System.out.println("Fetch Id count:" + count);
		List<Uploaddata> uploaddatas = uploaddataRepository.getIDByLimit(count);
		if (uploaddatas.size() > 0) {
			for (int i = 0; i < uploaddatas.size(); i++) {
				Long engagementId = uploaddatas.get(i).getEid();

				System.out.println("ID:" + uploaddatas.get(i).getId() + " -- EID:" + engagementId);

				// data = engagementId.toString() + "?hapikey=" + hubspotKey;
				String data = engagementId + "?hapikey=" + hubspotKey;
				try {
					String agentsClientResponse = thirdPartyClient.restClientForGet(null, data,
							detailsByEnganementsIdURL);
					HubspotResponse hubspotResponse = mapper.readValue(agentsClientResponse, HubspotResponse.class);
					if (hubspotResponse != null && hubspotResponse.getAssociations().getContactIds().size() > 0
							&& hubspotResponse.getAssociations().getContactIds().get(0) != null) {
						String data1 = hubspotResponse.getAssociations().getContactIds().get(0) + "/profile?hapikey="
								+ hubspotKey;

						String custDetails = thirdPartyClient.restClientForGet(null, data1, detailsByContactIdURL);
						Contact contact = mapper.readValue(custDetails, Contact.class);
						if (contact.getIdentityProfiles().size() > 0
								&& contact.getIdentityProfiles().get(0).getIdentities().size() > 0
								&& contact.getIdentityProfiles().get(0).getIdentities().get(0).getType()
										.contentEquals("EMAIL")) {

							JSONObject jsonObject = new JSONObject();
							jsonObject.put("EmailAddress",
									contact.getIdentityProfiles().get(0).getIdentities().get(0).getValue());
							jsonObject.put("ActivityEvent", 166l);
							jsonObject.put("ActivityNote", hubspotResponse.getEngagement().getBodyPreview());
							jsonObject.put("ActivityDateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
									.format(new Timestamp(new java.util.Date().getTime())));

							String LStaskResponse = thirdPartyClient.restClientForPost(jsonObject.toJSONString(), null,
									lsPushActivityURL, hubspotKey);
							System.out.println(LStaskResponse);

						}
					}
				} catch (Exception e) {
					// i++;
					// TODO: handle exception
				}

			}
		}
		count += 15;

	}

	public String pushTask(TaskPushRequest pushRequest) throws JsonMappingException, JsonProcessingException {
		String data = pushRequest.getEmail() + "/profile?hapikey=" + hubspotKey;

		String agentsClientResponse = thirdPartyClient.restClientForGet(null, data, detailsByEmailIdURL);

		Contact contact = mapper.readValue(agentsClientResponse, Contact.class);

		HubspotResponse hubspotResponse = new HubspotResponse();

		EngagementResponse engagementResponse = new EngagementResponse();
		engagementResponse.setActive(true);
		engagementResponse.setType("TASK");
		engagementResponse.setOwnerId(33196289l);
		engagementResponse.setTimestamp(System.currentTimeMillis());
		hubspotResponse.setEngagement(engagementResponse);

		AssociationsResponse associations = new AssociationsResponse();
		List<Long> contactIds = new ArrayList<Long>();
		contactIds.add(contact.getCanonicalVid());
		associations.setContactIds(contactIds);
		hubspotResponse.setAssociations(associations);

		Request metadata = new Request();
		metadata.setBody(pushRequest.getBody());
		hubspotResponse.setMetadata(metadata);

		String taskResponse = thirdPartyClient.restClientForPost(mapper.writeValueAsString(hubspotResponse), null,
				taskEngagementurl + hubspotKey, hubspotKey);
		return taskResponse;

	}
}
