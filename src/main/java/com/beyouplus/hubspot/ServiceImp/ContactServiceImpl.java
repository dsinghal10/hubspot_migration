package com.beyouplus.hubspot.ServiceImp;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.beyouplus.hubspot.Client.ThirdPartyClient;
import com.beyouplus.hubspot.Repository.ContactDetailsRepository;
import com.beyouplus.hubspot.Service.ContactService;
import com.beyouplus.hubspot.json.ContactsDetails;
import com.beyouplus.hubspot.json.Request;
import com.beyouplus.hubspot.model.Contacts;
import com.beyouplus.hubspot.utility.MiliToTimestamp;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ContactServiceImpl implements ContactService {
	@Autowired
	ContactDetailsRepository contactDetailsRepository;
	@Autowired
	ThirdPartyClient thirdPartyClient;
	@Value("${fetch.details}")
	private String detailsURL;
	@Value("${fetch.details.vidOffset}")
	private String detailsByVidOffsetURL;
	@Autowired
	MiliToTimestamp miliToTimestamp;
	@Autowired
	PushNotesLS pushNotesLS;

	// @Scheduled(cron = "${scheduler.metrics}")
	@Override
	public int saveContactDetails(Request jsonrequest) throws Exception {
		int savedContacts = 0;
		int savedVidAPIContacts = 0;
		String detailsClientResponse = thirdPartyClient.restClientForGet(jsonrequest, jsonrequest.getCount(),
				detailsURL);
		System.out.println("\n Third Party Details API Response:--> " + detailsClientResponse);
		ContactsDetails contactsDetails = new ContactsDetails();
		ObjectMapper mapper = new ObjectMapper();
		contactsDetails = mapper.readValue(detailsClientResponse, ContactsDetails.class);
		savedContacts = modelBuilder(contactsDetails);
		savedVidAPIContacts = saveContactDetailsByVidOffset(jsonrequest, contactsDetails.getHasMore(),
				contactsDetails.getVidOffset());
		return (savedContacts + savedVidAPIContacts);
	}

	int saveContactDetailsByVidOffset(Request jsonrequestbody, boolean hasMore, Long vidOffset) throws Exception {
		boolean currentStatus = hasMore;
		Long currentOffset = vidOffset;
		int noOfSavedVidAPIContacts = 0;
		while (currentStatus) {
			System.out.println("Status-->" + currentStatus + "  " + "Offset-->" + currentOffset);
			String detailsClientVidOffsetResponse = thirdPartyClient.restClientForGet(jsonrequestbody,
					currentOffset.toString(), detailsByVidOffsetURL);
			System.out.println("\n Third Party VidOffset API Response:--> " + detailsClientVidOffsetResponse);
			ContactsDetails contactsDetails = new ContactsDetails();
			ObjectMapper mapper = new ObjectMapper();
			contactsDetails = mapper.readValue(detailsClientVidOffsetResponse, ContactsDetails.class);
			noOfSavedVidAPIContacts = modelBuilder(contactsDetails);
			currentStatus = contactsDetails.getHasMore();
			currentOffset = contactsDetails.getVidOffset();
		}
		return noOfSavedVidAPIContacts;
	}

	int modelBuilder(ContactsDetails contactsDetails) {

		int contactSkipCounter = 0;
		int fetchAllContactCount = contactsDetails.getContacts().size();

		for (int i = 0; i < contactsDetails.getContacts().size(); i++) {

			if (contactsDetails.getContacts().get(i).getCanonicalVid() != null) {
				Contacts contactsDb = contactDetailsRepository
						.findBycustomerId(contactsDetails.getContacts().get(i).getCanonicalVid());
				if (contactsDb != null) {
					contactSkipCounter++;
					continue;
				}
			}
			Contacts contacts = new Contacts();
			if (contactsDetails.getContacts().get(i).getCanonicalVid() != null) {
				contacts.setCustomerId(contactsDetails.getContacts().get(i).getCanonicalVid());
			}
			if (contactsDetails.getContacts().get(i).getAddedAt() != null) {

				contacts.setCustomerCreationDate(
						miliToTimestamp.parseMilitoTimestamp(contactsDetails.getContacts().get(0).getAddedAt()));
			}
			if (contactsDetails.getContacts().get(i).getContactProperties().getFirstname() != null) {
				contacts.setFirstName(
						contactsDetails.getContacts().get(i).getContactProperties().getFirstname().getValue());
			}

			if (contactsDetails.getContacts().get(i).getContactProperties().getLastname() != null) {
				contacts.setLastName(
						contactsDetails.getContacts().get(i).getContactProperties().getLastname().getValue());
			}
			if (contactsDetails.getContacts().get(i).getContactProperties().getLastmodifieddate() != null) {
				Long lastModifiedDate = Long.parseLong(
						contactsDetails.getContacts().get(i).getContactProperties().getLastmodifieddate().getValue());
				contacts.setLastUpdatedDate(miliToTimestamp.parseMilitoTimestamp(lastModifiedDate));

			}
			if (contactsDetails.getContacts().get(i).getIdentityProfiles().get(0).getIdentities().get(0) != null) {
				contacts.setCustomerEmail(contactsDetails.getContacts().get(i).getIdentityProfiles().get(0)
						.getIdentities().get(0).getValue());
			}
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			contacts.setCreatedDate(timestamp);
			contacts.setCreatedBy("Admin");
			contacts.setLastUpdatedDate(timestamp);
			contacts.setLastUpdatedBy("Admin");

			contactDetailsRepository.saveAndFlush(contacts);
		}
		return (fetchAllContactCount - contactSkipCounter);
	}

}
