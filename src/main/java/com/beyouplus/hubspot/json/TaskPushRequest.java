package com.beyouplus.hubspot.json;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class TaskPushRequest {
	@NotBlank(message = "Task body is mandatory")
	String body;
	@Email
	@NotBlank(message = "Email is mandatory")
	String email;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
