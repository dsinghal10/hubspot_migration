package com.beyouplus.hubspot.json;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LSTActivityPushRequest {

	@JsonProperty("EmailAddress")
	String EmailAddress;
	@JsonProperty("ActivityEvent")
	Long ActivityEvent;
	@JsonProperty("ActivityNote")
	String ActivityNote;
	@JsonProperty("ActivityDateTime")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
	Timestamp ActivityDateTime;

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public Long getActivityEvent() {
		return ActivityEvent;
	}

	public void setActivityEvent(Long activityEvent) {
		ActivityEvent = activityEvent;
	}

	public String getActivityNote() {
		return ActivityNote;
	}

	public void setActivityNote(String activityNote) {
		ActivityNote = activityNote;
	}

	public Timestamp getActivityDateTime() {
		return ActivityDateTime;
	}

	public void setActivityDateTime(Timestamp activityDateTime) {
		ActivityDateTime = activityDateTime;
	}

}
