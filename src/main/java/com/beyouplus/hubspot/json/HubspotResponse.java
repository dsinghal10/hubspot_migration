package com.beyouplus.hubspot.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HubspotResponse {
	EngagementResponse engagement;
	AssociationsResponse associations;
	Request metadata;

	public Request getMetadata() {
		return metadata;
	}

	public void setMetadata(Request metadata) {
		this.metadata = metadata;
	}

	public EngagementResponse getEngagement() {
		return engagement;
	}

	public void setEngagement(EngagementResponse engagement) {
		this.engagement = engagement;
	}

	public AssociationsResponse getAssociations() {
		return associations;
	}

	public void setAssociations(AssociationsResponse associations) {
		this.associations = associations;
	}

}
