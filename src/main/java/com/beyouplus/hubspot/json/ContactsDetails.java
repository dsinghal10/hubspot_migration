package com.beyouplus.hubspot.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactsDetails {
	private List<Contact> contacts;
	@JsonProperty("has-more")
	private boolean hasMore;
	@JsonProperty("vid-offset")
	private Long vidOffset;
	@JsonProperty("time-offset")
	private Long timeOffset;

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public boolean getHasMore() {
		return hasMore;
	}

	public void setHasMore(boolean hasMore) {
		this.hasMore = hasMore;
	}

	public Long getVidOffset() {
		return vidOffset;
	}

	public void setVidOffset(Long vidOffset) {
		this.vidOffset = vidOffset;
	}

	public Long getTimeOffset() {
		return timeOffset;
	}

	public void setTimeOffset(Long timeOffset) {
		this.timeOffset = timeOffset;
	}

}
