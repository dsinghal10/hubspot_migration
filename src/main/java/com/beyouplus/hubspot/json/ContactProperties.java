package com.beyouplus.hubspot.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactProperties {
	public FirstName firstname;
	public LastName lastname;
	public ModifiedDate lastmodifieddate;

	public HubspotOwnerIdRequest hubspot_owner_id;

	public HubspotOwnerIdRequest getHubspot_owner_id() {
		return hubspot_owner_id;
	}

	public void setHubspot_owner_id(HubspotOwnerIdRequest hubspot_owner_id) {
		this.hubspot_owner_id = hubspot_owner_id;
	}

	public ModifiedDate getLastmodifieddate() {
		return lastmodifieddate;
	}

	public void setLastmodifieddate(ModifiedDate lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}

	public FirstName getFirstname() {
		return firstname;
	}

	public void setFirstname(FirstName firstname) {
		this.firstname = firstname;
	}

	public LastName getLastname() {
		return lastname;
	}

	public void setLastname(LastName lastname) {
		this.lastname = lastname;
	}

}
