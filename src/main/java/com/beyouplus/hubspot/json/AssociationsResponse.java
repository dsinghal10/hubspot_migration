package com.beyouplus.hubspot.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssociationsResponse {
	List<Long> contactIds = new ArrayList<Long>();

	public List<Long> getContactIds() {
		return contactIds;
	}

	public void setContactIds(List<Long> contactIds) {
		this.contactIds = contactIds;
	}

}
