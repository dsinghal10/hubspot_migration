
package com.beyouplus.hubspot.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {

	@JsonProperty("properties")
	public ContactProperties contactProperties;

	@JsonProperty("identity-profiles")
	private List<ContactIdentityProfiles> identityProfiles;

	public Long vid;
	public Long addedAt;
	@JsonProperty("portal-id")
	public Long portalId;
	@JsonProperty("profile-token")
	public String profileToken;
	@JsonProperty("profile-url")
	public String profileUrl;
	@JsonProperty("canonical-vid")
	public Long canonicalVid;

	public Long getVid() {
		return vid;
	}

	public void setVid(Long vid) {
		this.vid = vid;
	}

	public Long getAddedAt() {
		return addedAt;
	}

	public void setAddedAt(Long addedAt) {
		this.addedAt = addedAt;
	}

	public Long getPortalId() {
		return portalId;
	}

	public void setPortalId(Long portalId) {
		this.portalId = portalId;
	}

	public String getProfileToken() {
		return profileToken;
	}

	public void setProfileToken(String profileToken) {
		this.profileToken = profileToken;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public Long getCanonicalVid() {
		return canonicalVid;
	}

	public void setCanonicalVid(Long canonicalVid) {
		this.canonicalVid = canonicalVid;
	}

	public ContactProperties getContactProperties() {
		return contactProperties;
	}

	public void setContactProperties(ContactProperties contactProperties) {
		this.contactProperties = contactProperties;
	}

	public List<ContactIdentityProfiles> getIdentityProfiles() {
		return identityProfiles;
	}

	public void setIdentityProfiles(List<ContactIdentityProfiles> identityProfiles) {
		this.identityProfiles = identityProfiles;
	}

}