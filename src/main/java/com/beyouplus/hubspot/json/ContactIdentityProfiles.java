package com.beyouplus.hubspot.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactIdentityProfiles {
	public Long vid;
	public List<ContactIdentity> identities;

	public Long getVid() {
		return vid;
	}

	public void setVid(Long vid) {
		this.vid = vid;
	}

	public List<ContactIdentity> getIdentities() {
		return identities;
	}

	public void setIdentities(List<ContactIdentity> identities) {
		this.identities = identities;
	}

}
